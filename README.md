# RVMT

## Description

Scripts written by Joint Genome Institute researchers for the [RVMT](https://riboviria.org/) project. The remaining code used throughout the project can be found in the [main GitHub repository](https://github.com/UriNeri/RVMT).

## Scripts

- `protein_clustering.smk`: Clusters proteins in an input FASTA file using MMSeqs2 and create an HH-Suite database from the results.
- `parse_hhr.py`: Convert an HHblits/HHSearch output into a tabular file.
- `get_polyproteins.ipynb`: Identify putative polyprotein profiles by flagging the profiles that encompassed at least two other non-overlapping profiles.
- `get_clan_membership.ipynb`: Clusters protein profiles into clans of highly connected domains.

from pathlib import Path
from Bio import SeqIO

ID = Path(config["input"]).stem

def aggregate_input(wildcards):
    checkpoint_output = checkpoints.cluster_membership2.get(**wildcards).output[0]
    return expand("cluster_alignments/{cluster}.fasta", cluster=glob_wildcards(
        Path(checkpoint_output).joinpath("{cluster}.txt")).cluster)

rule all:
    input: aggregate_input

rule clean_fasta:
    input: config["input"]
    output: temp(f"{ID}_cleaned.fasta")
    run:
        f = open(str(output), "w")
        seq_id_list = set()
        for record in SeqIO.parse(str(input), "fasta"):
            seq_id = record.id.replace(",", "_").replace("\"", "_").replace("'", "_")
            if seq_id in seq_id_list:
                seq_id = seq_id + "_2"
            f.write(f">{seq_id}\n{record.seq}\n")
            seq_id_list.add(seq_id)
        f.close()

rule precluster:
    input: f"{ID}_cleaned.fasta"
    output:
        temp(f"{ID}_precluster_rep_seq.fasta"),
        temp(f"{ID}_precluster_all_seqs.fasta"),
        temp(f"{ID}_precluster_cluster.tsv"),
        temp(directory(f"{ID}_preclust_tmp"))
    params:
        precluster_min_seq_id=config.get("precluster_min_seq_id", 0.95),
    threads: config.get("threads", 16)
    shell:
        """
        mmseqs easy-linclust --threads {threads} --kmer-per-seq 100 -c 1.0 \
        --cluster-mode 2 --cov-mode 1 --min-seq-id {params.precluster_min_seq_id} \
        {input} {ID}_precluster {output[3]}
        """

rule sequencedb:
    input: f"{ID}_precluster_rep_seq.fasta"
    output: temp(directory(f"{ID}_seqdb"))
    shell:
        """
        mkdir {output}
        mmseqs createdb {input} {output}/{output}
        """

rule cluster:
    input: f"{ID}_seqdb"
    output:
        temp(directory(f"{ID}_clustdb")),
        temp(directory(f"{ID}_clust_tmp"))
    params:
        sensitivity=config.get("sensitivity", 7.5),
        min_aln_cov=config.get("min_aln_cov", 0.8),
        min_eval = config.get("min_eval", 1e-3),
        cluster_min_seq_id = config.get("cluster_min_seq_id", 0.25)
    threads: config.get("threads", 16)
    shell:
        """
        mkdir {output[0]}
        mmseqs cluster --threads {threads} -s {params.sensitivity} \
        -e {params.min_eval} -c {params.min_aln_cov} --cov-mode 0 \
        --cluster-mode 0 --max-seqs 5000 --min-seq-id {params.cluster_min_seq_id} \
        {input}/{input} {output[0]}/{output[0]} {output[1]}
        """

rule createtsv:
    input:
        f"{ID}_seqdb",
        f"{ID}_clustdb"
    output: f"{ID}_clusters.tsv"
    shell:
        """
        mmseqs createtsv --full-header 1 {input[0]}/{input[0]} \
        {input[0]}/{input[0]} {input[1]}/{input[1]} {output}
        sed -i.bak -e 's/^\"//' -e 's/\"\t\"/\t/' -e 's/\"$//' {output}
        rm {output}.bak
        """

checkpoint cluster_membership2:
    input: f"{ID}_clusters.tsv"
    output: directory("cluster_membership")
    params:
        min_cluster_size=config.get("min_cluster_size", 2),
    shell:
        """
        Rscript cluster_membership2.R {input} {params.min_cluster_size} {threads}
        """

rule mafft:
    input:
        "cluster_sequences/{cluster}.faa"
    output: "cluster_alignments/{cluster}.faa"
    threads: config.get("threads", 4)
    shell:
        """
        mafft --quiet --anysymbol --thread {threads} --auto {input} | \
        seqkit seq --threads {threads} -w 0 > {output}
        """
rule Pre_HHdbmaker:
    input:
        "cluster_alignments/{cluster}.fasta"
    output: "tmpmsa/{cluster}.a3m"
    threads: config.get("threads", 1)
    shell:
        """
        mkdir tmpmsa hhdb
        reformat.pl {input} {output}
        """
rule HHdbmaker:
    input:
        f"tmpmsa"
    output: directory("hhdb")
    threads: config.get("threads", 4)
    shell:
        """
        cd tmpmsa
        ffindex_build -s ../hhdb/db_msa.ff{data,index} .
        cd ../hhdb/
        mpirun -np {threads} ffindex_apply db_msa.ff{data,index} \
        -i db_hhm.ffindex -d db_hhm.ffdata -- hhmake -i stdin -o stdout -v 0
        mpirun -np 1 cstranslate -x 0.3 -c 4 -I a3m -i db_msa -o db_cs219 -f
        sort -k3 -n -r db_cs219.ffindex | cut -f1 > sorting.dat
        ffindex_order sorting.dat db_hhm.ff{data,index} db_hhm_ordered.ff{data,index}
        mv db_hhm_ordered.ffindex db_hhm.ffindex
        mv db_hhm_ordered.ffdata db_hhm.ffdata
        ffindex_order sorting.dat db_msa.ff{data,index} db_a3m_ordered.ff{data,index}
        mv db_a3m_ordered.ffindex db_a3m.ffindex
        mv db_a3m_ordered.ffdata db_a3m.ffdata
        cd ../
        """



